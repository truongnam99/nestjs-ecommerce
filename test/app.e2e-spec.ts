// require('dotenv').config({
//   path: '../.env.stag',
// });
import 'dotenv/config';
import { registerDto } from './../dist/auth/auth.dto.d';
import * as request from 'supertest';
import { HttpStatus } from '@nestjs/common';
import * as mongoose from 'mongoose';

const app = 'http://localhost:3000';

// beforeAll(async () => {
//   await mongoose.connect(process.env.MONGO_URI);
//   await mongoose.connection.db.dropDatabase();
// });

// afterAll(async done => {
//   await mongoose.disconnect(done);
// });

describe('AppController (e2e)', () => {
  it('/ (GET)', () => {
    return request(app)
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
});

describe('AUTH', () => {
  it('should register', () => {
    const user: registerDto = {
      username: 'username',
      password: 'password',
    };

    return request(app)
      .post('/auth/register')
      .set('Accept', 'application/json')
      .send(user)
      .expect(HttpStatus.CREATED);
  });
});
