# Project Description

E-Commerce Web Application

# Project Learning Goals

- Role Based Authentication
- NoSQL Database, MongoDB
- Explore Testing
- Explore Deployment/DevOps
- React/Redux frontend in Typescript
- React Hooks
- SSR with NextJS

# Start Project

- yarn

- yarn start
