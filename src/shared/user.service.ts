import { registerDto, LoginDto } from './../auth/auth.dto';
import { User } from './../types/user';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private userModel: Model<User>) {}

  sanitizeUser(user: User) {
    const sanitize = user.toObject();
    delete sanitize['password'];
    return sanitize;
  }

  async create(userDto: registerDto) {
    const { username } = userDto;
    const user = await this.userModel.findOne({ username });
    if (user) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    const createUser = new this.userModel(userDto);
    await createUser.save();
    return this.sanitizeUser(createUser);
  }

  async findByLogin(userDto: LoginDto) {
    const { username, password } = userDto;
    const user = await this.userModel.findOne({ username });
    if (!user)
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);

    if (await bcrypt.compare(password, user.password)) {
      console.log(this.sanitizeUser(user));
      return this.sanitizeUser(user);
    } else {
      throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
    }
  }

  async findByPayload(payload: any) {
    const { username } = payload;
    return await this.userModel.findOne({ username });
  }
}
