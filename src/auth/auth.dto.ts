export interface LoginDto {
  username: string;
  password: string;
}

export interface registerDto {
  username: string;
  password: string;
  seller?: boolean;
}
