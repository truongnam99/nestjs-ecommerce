import { UserService } from './../shared/user.service';
import { Injectable } from '@nestjs/common';
import { sign } from 'jsonwebtoken';

@Injectable()
export class AuthService {
  constructor(private UserService: UserService) {}

  async signPayload(payload: any) {
    return sign(payload, process.env.SECRET_KEY, {
      expiresIn: process.env.EXPIRES_TIME,
    });
  }

  async validateUser(payload: any) {
    return await this.UserService.findByPayload(payload);
  }
}
