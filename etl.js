const axios = require('axios');

(async function() {
  const {
    data: { token },
  } = await axios.post('http://localhost:3000/auth/login', {
    username: 'truongnam',
    password: 'truongnam',
  });

  const { data } = await axios.get(`http://localhost:3000/auth`, {
    headers: { authorization: `Bearer ${token}` },
  });
  console.log(token);
})();
